<?php
/*
Plugin Name: CU Featured Testies
Version: 0.1
Description: Custom Visual Composer addon to display a small featured testimonials slider on CardoneUniversity.com website.
Author: Elvis Morales
Author URI: http://elvismdev.io
Plugin URI: https://bitbucket.org/grantcardone/cu-featured-testies
Text Domain: cu-featured-testies
*/
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if ( !function_exists( 'cu_featured_testies' ) ) {
	function cu_featured_testies( $atts, $content = NULL ) {

		wp_enqueue_style( 'cardone-ionicons', get_stylesheet_directory_uri() . '/assets/Ionicons/css/ionicons.min.css' );

		extract( shortcode_atts( array(
			'cat'		 => '',
			'items'		 => '3'
			), $atts ) );
		
		
		if ( !empty( $cat ) ) {
			
			$category =	array(
				'taxonomy' => 'professors_tax',
				'field' => 'slug',
				'terms' => $cat,
				'operator' => 'IN'
				);
		} else {
			$category = NULL;
		}
		
		global $post;

		$args = array(
			'post_type' => 'testimonials',
			'posts_per_page' => $items,
			'order'          => 'DESC',
			'orderby'        => 'date',
			'post_status'    => 'publish',
			'tax_query' => array(
				'relation' => 'AND',
				$category
				)
			);

		query_posts( $args );

		global $wp_query;
		
		

		
		ob_start();
		?>
		<?php if( have_posts() ) : ?>
			<script type="text/javascript">
				jQuery( function( $ ) {
					var $slider = $( ".cu-featured-testies .flexslider" );

					$slider.flexslider({
						animation: "slide",
						controlNav : false,
						directionNav: true,
						slideshow: false,
						pauseOnHover: true
					});
				});
			</script>
			<div class="cu-featured-testies">
				<div class="flexslider">
					<ul class="slides">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php
							$comp_name = types_render_field( "company-name", array( "output"=>"raw" ) );
							$comp_website = types_render_field( "company-website", array( "output"=>"raw" ) );
							$twitter_handle = types_render_field( "twitter-handle", array( "output"=>"raw" ) );
							?>
							<li>
								<div class="cu-featured-testies-content">
									<div class="row">
										<div class="col-md-2 v-center">
											<div class="content">
												<img class="stars" src="<?php echo get_stylesheet_directory_uri(); ?>/images/5stars.png" />
											</div>
										</div>
										<div class="col-md-2 testi-pic">
											<?php if ( ( function_exists( 'has_post_thumbnail' ) ) && ( has_post_thumbnail() ) ) : ?>
												<figure>
													<?php the_post_thumbnail(); ?>
												</figure>
											<?php endif; ?>
										</div>
										<div class="col-md-8 testi-content v-center">
											<div class="content">
												<p>“<?php echo get_the_content(); ?>”</p>
												<div class="testi-meta">
													<span class="author">- <?php the_title(); ?></span>
													<?php if ( $twitter_handle ) : ?>
														<span class="separator"> | </span><span class="twitter"><a target="_blank" href="https://twitter.com/<?php printf( "%s", $twitter_handle ); ?>">@<?php printf( "%s", $twitter_handle ); ?></a></span>
													<?php endif; ?>
													<?php if ( $comp_name ) : ?>
														<span class="separator"> | </span><span class="website"><?php if ( $comp_website ) : ?><a target="_blank" href="<?php printf( "%s", $comp_website ); ?>"><?php endif; ?><?php printf( "%s", $comp_name ); ?><?php if ( $comp_website ) : ?></a><?php endif; ?></span>
													<?php endif; ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</li>
						<?php endwhile; 
						wp_reset_query();
						?>
					</ul>
				</div>
			</div>
		<?php endif; ?>

		

		<?php
		return ob_get_clean();	
	}
	add_shortcode( 'cu_featured_testies','cu_featured_testies' );
}

// register shortcode to VC

add_action( 'init', 'cu_featured_testies_vcmap' );

if ( ! function_exists( 'cu_featured_testies_vcmap' ) ) {
	function cu_featured_testies_vcmap() {
		vc_map( array(
			'name'					=> 'CU Featured Testies',
			'description'			=> 'Display a small testimonials slider',
			'base'					=> "cu_featured_testies",
			'class'					=> "cu_featured_testies",
			'category'				=> 'My University',
			'icon' 					=> "icon-wpb-sd-testimonials",
			'admin_enqueue_css' => get_template_directory_uri() . '/framework/inc/vc/assets/css/sd-vc-admin-styles.css',
			'front_enqueue_css' => get_template_directory_uri() . '/framework/inc/vc/assets/css/sd-vc-admin-styles.css',
			'params'				=> array(
				array(
					'type'			=> 'textfield',
					'class'			=> '',
					'heading'		=> 'Professor Category',
					'param_name'	=> 'cat',
					'value'			=> '',
					'description'	=> 'Enter the slug of the professors testimonials category. Eg. jhon-doe.',
					),
				array(
					'type'			=> 'textfield',
					'class'			=> '',
					'heading'		=> 'Number of items to display',
					'param_name'	=> 'items',
					'value'			=> '3',
					'description'	=> 'Enter the number of items to display (default is 3)',
					)
				)
			));
}
}